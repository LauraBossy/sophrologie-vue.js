import './assets/css/style.css'

import { createApp } from 'vue'
import router from './router/router'
import App from './App.vue'

// Rendre un composant utilisable partout dans l'app
// import TodoDeleteButton from './components/TodoDeleteButton.vue'
// //Enregistrez le composant globalement
// App.component('TodoDeleteButton', TodoDeleteButton)

createApp(App)
    .use(router)
    .mount('#app')
