import { createMemoryHistory, createRouter } from 'vue-router'

import Home from '../components/Home/Home.vue'
import About from '../components/About/About.vue'
// import Presta from '../components/Presta/Presta.vue'
import Contact from '../components/Contact/Contact.vue'
// import Present from '../components/Present/Present.vue'

const routes = [
  { path: '/', component: Home },
  { path: '/about', component: About },
  // { path: '/presta', component: Home_1 },
  { path: '/contact', component: Contact },
  // { path: '/present', component: Home_1 },
]

const router = createRouter({
  history: createMemoryHistory(),
  routes,
})

export default router